package com.upwork.rest;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.upwork.rest.dao.AttendanceDAO;
import com.upwork.rest.model.CheckinRequest;
import com.upwork.rest.model.LocationRequest;
import com.upwork.rest.model.ScheduleResponse;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)

// Basic implementation for Junit testing of services. If required, can be replaced with more detailed Mockito test cases
public class QRAttendanceDemoApplicationTests 
{   
    @LocalServerPort
    int randomServerPort;
    
    //Timeout value in milliseconds
    int timeout = 10_000;
    
    public RestTemplate restTemplate;
    private static final Logger log = LogManager.getLogger(QRAttendanceDemoApplicationTests.class);
    
    @Before
    public void setUp() {
        restTemplate = new RestTemplate(getClientHttpRequestFactory());
    }
    
    private HttpComponentsClientHttpRequestFactory getClientHttpRequestFactory() 
    {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory
                          = new HttpComponentsClientHttpRequestFactory();
        //Connect timeout
        clientHttpRequestFactory.setConnectTimeout(timeout);
        
        //Read timeout
        clientHttpRequestFactory.setReadTimeout(timeout);
        return clientHttpRequestFactory;
    }
    
    @SuppressWarnings("deprecation")
	@Test
    public void testGetSchedule_success() throws URISyntaxException 
    {
    	log.info("get schdule test");
        final String baseUrl = "http://localhost:"+randomServerPort+"/attendance/schedule?classId=1&studentId=s1";
        URI uri = new URI(baseUrl);
        restTemplate.getInterceptors().add(
        		  new BasicAuthorizationInterceptor("admin", "password"));
        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
        
        //Verify request succeed
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertEquals(true, result.getBody().contains("scheduleList"));
    }
    
    @SuppressWarnings("deprecation")
	@Test
    public void testLocationMatch_success() throws URISyntaxException 
    {
    	log.info("location match test");
        final String baseUrl = "http://localhost:"+randomServerPort+"/attendance/location";
        URI uri = new URI(baseUrl);
        restTemplate.getInterceptors().add(
        		  new BasicAuthorizationInterceptor("admin", "password"));
        LocationRequest locReq = new LocationRequest();
        locReq.setClassLocation("building1");
        locReq.setGeoLat("0.0");
        locReq.setGeoLong("0.0");
        ResponseEntity<String> result = restTemplate.postForEntity(uri, locReq, String.class);
        
        //Verify request succeed
        Assert.assertEquals(200, result.getStatusCodeValue());
        Assert.assertEquals(true, result.getBody().contains("Location matched"));
    }
    
    @SuppressWarnings("deprecation")
   	@Test
       public void testCheckin_failure() throws URISyntaxException 
       {
    	   log.info("checkin failure test");
           final String baseUrl = "http://localhost:"+randomServerPort+"/attendance/checkin";
           URI uri = new URI(baseUrl);
           restTemplate.getInterceptors().add(
           		  new BasicAuthorizationInterceptor("admin", "password"));
           CheckinRequest req = new CheckinRequest();
           req.setSessionId(UUID.randomUUID());
           req.setClassId("c1");
           req.setStudentId("s1");
           ResponseEntity<String> result = restTemplate.postForEntity(uri, req, String.class);
           log.info(result.getBody());
           
           //Verify request succeed
           Assert.assertEquals(200, result.getStatusCodeValue());
           Assert.assertEquals(true, result.getBody().contains("Invalid session"));
       }
    
    
    @SuppressWarnings("deprecation")
   	@Test
       public void testCheckin_success() throws URISyntaxException 
	{
    	log.info("checkin success test");
		String baseUrl = "http://localhost:" + randomServerPort + "/attendance/schedule?classId=1&studentId=s1";
		URI uri = new URI(baseUrl);
		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("admin", "password"));
		ResponseEntity<ScheduleResponse> scheduleResult = restTemplate.getForEntity(uri, ScheduleResponse.class);
		log.info(scheduleResult.getBody().toString());
		UUID session_id = scheduleResult.getBody().getSessionId();

		baseUrl = "http://localhost:" + randomServerPort + "/attendance/checkin";
		uri = new URI(baseUrl);
		CheckinRequest req = new CheckinRequest();
		req.setSessionId(session_id);
		req.setClassId("c1");
		req.setStudentId("s1");
		ResponseEntity<String> result = restTemplate.postForEntity(uri, req, String.class);
		log.info(result.getBody());

		// Verify request succeed
		Assert.assertEquals(200, result.getStatusCodeValue());
		Assert.assertEquals(true, result.getBody().contains("Student checked in successfully"));
	}
    
	
}
