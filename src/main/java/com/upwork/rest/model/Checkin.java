package com.upwork.rest.model;

import java.util.Date;
import java.util.UUID;

public class Checkin {
	
	private UUID id;
	private String classId;
	private String studentId;
	private Date checkinDate;
	
	public Checkin(String classId, String studentId) {
		super();
		this.id = UUID.randomUUID();
		this.studentId = studentId;
		this.classId = classId;
		this.checkinDate = new Date();
	}
	
	
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public Date getCheckinDate() {
		return checkinDate;
	}
	public void setCheckinDate(Date checkinDate) {
		this.checkinDate = checkinDate;
	}
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	
	

}
