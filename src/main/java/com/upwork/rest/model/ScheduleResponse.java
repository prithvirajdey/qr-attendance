package com.upwork.rest.model;

import java.util.UUID;

public class ScheduleResponse {
	
	private Schedules schedules;
	private UUID sessionId;
	
	public Schedules getSchedules() {
		return schedules;
	}
	public void setSchedules(Schedules schedules) {
		this.schedules = schedules;
	}
	public UUID getSessionId() {
		return sessionId;
	}
	public void setSessionId(UUID sessionId) {
		this.sessionId = sessionId;
	}
	
	

}
