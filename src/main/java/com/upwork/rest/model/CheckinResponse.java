package com.upwork.rest.model;

import java.util.UUID;

public class CheckinResponse {
	
	private UUID sessionId;
	private String checkinStatus;
	
	public UUID getSessionId() {
		return sessionId;
	}
	public void setSessionId(UUID sessionId) {
		this.sessionId = sessionId;
	}
	public String getCheckinStatus() {
		return checkinStatus;
	}
	public void setCheckinStatus(String checkinStatus) {
		this.checkinStatus = checkinStatus;
	}
	
	

}
