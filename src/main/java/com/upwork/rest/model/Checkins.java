package com.upwork.rest.model;

import java.util.ArrayList;
import java.util.List;

public class Checkins {
	
	private List<Checkin> checkinsList;

	public List<Checkin> getCheckinsList() {
        if(checkinsList == null) {
        	checkinsList = new ArrayList<>();
        }
        return checkinsList;
    }

	public void setCheckinsList(List<Checkin> checkinsList) {
		this.checkinsList = checkinsList;
	}
	
	

}
