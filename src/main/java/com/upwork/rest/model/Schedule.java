package com.upwork.rest.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Schedule {

	public Schedule() {

	}
	
	public static Date mapTime(String time) {
		DateFormat formatter = new SimpleDateFormat("d-MMM-yyyy,HH:mm:ss aaa");
		Date date = null;
		try {
			date = formatter.parse(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return(date);
	}

	public Schedule(Integer id, String classroomId, String name, String location, String startTime, String endTime) {
		super();
		this.id = id;
		this.classroomId = classroomId;
		this.name = name;
		this.location = location;
		this.startTime = mapTime(startTime);
		this.endTime = mapTime(endTime);
		
	}

	private Integer id;
	private String classroomId;
	private String name;
	private String location;
	private Date startTime;
	private Date endTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClassroomId() {
		return classroomId;
	}

	public void setClassroomId(String classroomId) {
		this.classroomId = classroomId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", classroomId=" + classroomId + ", name=" + name + ", location=" + location
				+ ", startTime=" + startTime + ", endTime=" + endTime + "]";
	}
}
