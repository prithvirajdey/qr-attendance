package com.upwork.rest.model;

import java.util.UUID;

public class CheckinRequest {
	
	private String studentId;
	private UUID sessionId;
	private String classId;
	
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public UUID getSessionId() {
		return sessionId;
	}
	public void setSessionId(UUID sessionID) {
		this.sessionId = sessionID;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classID) {
		this.classId = classID;
	}
	
	

}
