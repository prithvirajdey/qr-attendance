package com.upwork.rest.controller;

import java.text.ParseException;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.upwork.rest.exception.ErrorResponse;
import com.upwork.rest.model.CheckinRequest;
import com.upwork.rest.model.CheckinResponse;
import com.upwork.rest.model.LocationRequest;
import com.upwork.rest.model.ScheduleResponse;
import com.upwork.rest.service.AttendanceService;

@RestController
@RequestMapping(path = "/attendance")
public class AttendanceController { //TODO messages can come from config files
	@Autowired
	private AttendanceService attendanceService;
	private static final Logger log = LogManager.getLogger(AttendanceController.class);

	@GetMapping(path = "/schedule", produces = "application/json")
	public ResponseEntity<Object> getSchedules(@RequestParam int classId, @RequestParam String studentId)
			throws ParseException // Service to get schedules
	{
		log.info("in get schedules for class "+classId+" student "+studentId);
		Date currentDate = new Date();
		ScheduleResponse resp = attendanceService.getSchedules(classId, studentId, currentDate);
		if (null != resp) {
			return new ResponseEntity<Object>(resp, HttpStatus.OK);
		} else {
			ErrorResponse errResp = new ErrorResponse("Internal error - could not get schedules", null);
			return new ResponseEntity<Object>(errResp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path = "/location", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> matchLocation(
			@RequestBody LocationRequest locReq) // Service to match geo location to prevent spoofing
	{
		log.info("in match locations ");
		if(attendanceService.matchLocation(locReq)) {
			return new ResponseEntity<Object>("Location matched", HttpStatus.OK);
		}else {
			ErrorResponse errResp = new ErrorResponse("Loaction does not match with class location", null);
			return new ResponseEntity<Object>(errResp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(path = "/checkin", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> checkin(
			@RequestBody CheckinRequest checkin) // Service to checkin student to class

	{
		log.info("in checkin");
		log.info(checkin.getStudentId());
		log.info(checkin.getSessionId());
		log.info(checkin.getClassId());

		if (null == checkin.getStudentId() || null == checkin.getSessionId() || null == checkin.getClassId()) {
			return new ResponseEntity<Object>("Invalid request - could not perform checkin",
					HttpStatus.INTERNAL_SERVER_ERROR);

		} else {
			CheckinResponse resp = attendanceService.checkin(checkin.getStudentId(), checkin.getSessionId(),
					checkin.getClassId());
			return new ResponseEntity<Object>(resp.getCheckinStatus(), HttpStatus.OK);
		}
	}
}
