package com.upwork.rest.service;

import java.text.ParseException;
import java.util.Date;
import java.util.UUID;

import com.upwork.rest.model.CheckinResponse;
import com.upwork.rest.model.LocationRequest;
import com.upwork.rest.model.ScheduleResponse;

public interface AttendanceService {
	
	// API for getting schedule
	public ScheduleResponse getSchedules(int classId, String studentId, Date date) throws ParseException;
	
	// API for checkin
	public CheckinResponse checkin(String studenId, UUID sessionId, String classId);
	
	// API for location matching
		public boolean matchLocation(LocationRequest req);
}
