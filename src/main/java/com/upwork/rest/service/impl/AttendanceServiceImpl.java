package com.upwork.rest.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.upwork.rest.dao.AttendanceDAO;
import com.upwork.rest.dao.CheckinDAO;
import com.upwork.rest.model.CheckinResponse;
import com.upwork.rest.model.LocationRequest;
import com.upwork.rest.model.ScheduleResponse;
import com.upwork.rest.model.Schedules;
import com.upwork.rest.service.AttendanceService;

@Service
public class AttendanceServiceImpl implements AttendanceService { // TODO messages can come from config files

	@Autowired
	private AttendanceDAO attendanceDao;

	@Autowired
	private CheckinDAO checkinDao;

	private static LoadingCache<String, UUID> checkinCache;
	
	private static final Logger log = LogManager.getLogger(AttendanceServiceImpl.class);

	// Implementing a simple cache to store session ids with student ids in order to
	// enable the 2pc
	static {

		checkinCache = CacheBuilder.newBuilder().maximumSize(100) // maximum 100 records can be cached
				.expireAfterAccess(15, TimeUnit.MINUTES) // cache will expire after 15 minutes of access
				.build(new CacheLoader<String, UUID>() { // build the cacheloader

					@Override
					public UUID load(String empId) throws Exception {
						// add the session id against the student id
						return UUID.randomUUID();
					}
				});
	}

	public static Date mapTime(String time) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("d-MMM-yyyy,HH:mm:ss aaa");
		Date date = null;
		date = formatter.parse(time);
		return (date);
	}

	@Override
	public ScheduleResponse getSchedules(int classId, String studentId, Date date) throws ParseException {
		log.info("in get schedule service");
		ScheduleResponse resp = new ScheduleResponse();
		// get the applicable schedule in the time frame
		Schedules schedules = attendanceDao.getSchedule(classId, date);
		resp.setSchedules(schedules);
		// No need to set a session if no matching classes
		if (schedules.getScheduleList().size() == 0) {
			return resp;
		}
		// add the the cache for maintaining session ids
		resp.setSessionId(checkinCache.getUnchecked(studentId));
		return resp;
	}

	@Override
	public CheckinResponse checkin(String studentId, UUID sessionId, String classId) {
		CheckinResponse resp = new CheckinResponse();

		// TODO - Additional business validation to validate class and student ID can be
		// added here
		log.info("in checkin service");
		UUID sessionCheck = checkinCache.getIfPresent(studentId);
		log.info("session in cache "+ sessionCheck);

		// check if same student had retrieved the schedule before
		if (sessionCheck == null || !sessionCheck.equals(sessionId)) {
			resp.setCheckinStatus("Invalid session - Could not check in");
		} else {
			resp.setCheckinStatus(checkinDao.checkin(classId, studentId));
		}
		return resp;
	}

	@Override
	public boolean matchLocation(LocationRequest req) {
		// TODO Pending implementation with Geo Location API services to match the user's geo location with the location of class stored in DB
		// Spoofing this to always true for now
		
		// ClassLocationObj classLocationObj = getClassLocationfromDB(req.getClassLocation);
		// boolean match = MatchGeoLocationsfromGoogleAPI (ClassLocationObj, req.getGeoLat, req.getGeoLang)
		// return match
		
		return true;
	}

}
