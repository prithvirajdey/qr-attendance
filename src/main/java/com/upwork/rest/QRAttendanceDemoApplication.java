package com.upwork.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication; 

@SpringBootApplication 
public class QRAttendanceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(QRAttendanceDemoApplication.class, args);
    }
}
