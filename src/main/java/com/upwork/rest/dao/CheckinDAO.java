package com.upwork.rest.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.upwork.rest.model.Checkin;
import com.upwork.rest.model.Checkins;

@Repository
public class CheckinDAO {
	
	private static Checkins list = new Checkins();
	private static final Logger log = LogManager.getLogger(CheckinDAO.class);
	
	// This is just to simulate a DB entry
	public String checkin(String classId, String studentId) {
		log.info("Persisiting checkin checkin DAO");
		Checkin checkin = new Checkin(classId, studentId);
		list.getCheckinsList().add(checkin);
		log.info("Check in entry successful");
		return("Student checked in successfully");
	}

}
