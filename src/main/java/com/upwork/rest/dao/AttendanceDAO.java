package com.upwork.rest.dao;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.upwork.rest.model.Schedule;
import com.upwork.rest.model.Schedules;

@Repository
public class AttendanceDAO {
	private static Schedules list = new Schedules();
	private static final Logger log = LogManager.getLogger(AttendanceDAO.class);

	static {
		list.getScheduleList().add(new Schedule(1, "c1", "Programming101", "building1", "09-Apr-2022,19:30:00 PM",
				"11-Apr-2022,17:00:00 PM"));
		list.getScheduleList().add(new Schedule(1, "c1", "Programming101", "building1", "12-Apr-2022,13:00:00 PM",
				"12-Apr-2022,14:00:00 PM"));
		list.getScheduleList().add(new Schedule(1, "c1", "Programming101", "building1", "13-Apr-2022,13:00:00 PM",
				"13-Apr-2022,14:00:00 PM"));
		list.getScheduleList().add(new Schedule(2, "c2", "English102", "building2", "13-Apr-2022,13:00:00 PM",
				"13-Apr-2022,14:00:00 PM"));

	}

	public Schedules getSchedule(int id, Date date) {
		log.info("Retrieving the chedules from DAO");
		Schedules checkinSchedule = new Schedules();
		for (Schedule s : list.getScheduleList()) {
			// return only those instances which are to start in 15 mins of the request time
			// and before the class ends
			if (s.getStartTime().getTime() - date.getTime() <= 15 * 60 * 1000
					&& s.getEndTime().getTime() > date.getTime() && id == s.getId()) {
				checkinSchedule.getScheduleList().add(s);
			}
		}
		return checkinSchedule;
	}

}
