# QR Attendance REST Services
# Current Working Branch - main

## Running in Local

1. Clone the git repo to local machine
2. Build and Install dependencies by running **mvn clean compile install** OR Open in IDE of choice
3. Run the spring boot project using **mvn spring-boot:run
4. Sample Postman collection is included in src/test/resources

